const express = require('express')
const db = require('../db')
const config = require('../config')
const utils = require('../utils')
const crypto = require('crypto-js')
const mailer = require('../mailer')
const uuid = require('uuid')
const fs = require('fs')
const path = require('path')
const jwt = require('jsonwebtoken')

const router = express.Router()

// ---------------------------------------
//                  GET
// ---------------------------------------

router.get('/listLeave', (request, response) => {
  const statement = `select * from apply_leave`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.get('/listMeeting', (request, response) => {
  const statement = `select * from meeting`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.get('/empQueries', (request, response) => {
  const statement = `select q.qId,q.queryTitle,q.queryDesc,q.queryStatus,e.empName from emp_queries q inner join employee e on e.empId=q.empId`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ---------------------------------------
//                  POST
// ---------------------------------------
router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const statement = `select empId, empName, role from employee where email = '${email}' and password = '${password}'`
  db.query(statement, (error, admins) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (admins.length == 0) {
        response.send({ status: 'error', error: 'admin does not exist' })
      } else {
        const admin = admins[0]
        const token = jwt.sign({ id: admin['empId'] }, config.secret)
        response.send(utils.createResult(error, {
          empName: admin['empName'],
          role: admin['role'],
          token: token
        }))
      }
    }
  })
})

router.post('/addNewEmployee', (request, response) => {
  const { empName, address, birth_date, gendor, email, mobile, password, role } = request.body

  let body = `Email=${email}<br> Password=${password}`

  const statement = `insert into employee (empName, address, birth_date, gendor,email,mobile,password,role) values (
    '${empName}', '${address}', '${birth_date}', '${gendor}', '${email}','${mobile}','${password}','${role}'
  )`
  db.query(statement, (error, data) => {

    mailer.sendEmail(email, `Welcome ${empName} you are new ${role}`, body, (error, info) => {
      console.log(error)
      console.log(info)
      response.send(utils.createResult(error, data))
    })

  })
})

router.post('/addMeeting', (request, response) => {
  const { meetingDate, meetingInfo, meetingStatus } = request.body
  const statement = `insert into meeting (meetingDate, meetingInfo,meetingStatus,adminId) values ('${meetingDate}','${meetingInfo}','${meetingStatus}','${request.id}')`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.post('/salary_slip/:empId', (request, response) => {
  const { empId } = request.params
  const leaveStatement = `Select count(leaveId) as count from apply_leave where leaveStatus='approved' and empId=${empId}`
  db.query(leaveStatement, (error, data) => {
    if (error) {
      response.send(utils.createError(error))
    } else {
      const leave = data[0]['count']
      const deleteStatement = `delete from salary_slip where empId=${empId}`
      db.query(deleteStatement, (error, data) => {
        if (error) {
          response.send(utils.createError(error))
        } else {
          console.log(leave)
          const insertStatement = `insert into salary_slip (noOfPresentDay,leaveCount,totalAmount,adminId,empId) values (30-${leave},${leave},(30-${leave})*800,${request.id},${empId})`
          db.query(insertStatement, (error, data) => {
            response.send(utils.createResult(error, data))
          })
        }
      })

    }
  })
})


// ---------------------------------------
//                  PUT
// ---------------------------------------

router.put('/approveLeave/:leaveId', (request, response) => {
  const { leaveId } = request.params
  const statement = `update apply_leave set leaveStatus='approved' where leaveId=${leaveId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.put('/rejectLeave/:leaveId', (request, response) => {
  const { leaveId } = request.params
  const statement = `update apply_leave set leaveStatus='reject' where leaveId=${leaveId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})


// ---------------------------------------
//                  DELETE
// ---------------------------------------




module.exports = router