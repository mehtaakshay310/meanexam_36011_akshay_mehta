const express = require('express')
const db = require('../db')
const config = require('../config')
const utils = require('../utils')
const crypto = require('crypto-js')
const mailer = require('../mailer')
const uuid = require('uuid')
const fs = require('fs')
const path = require('path')
const jwt = require('jsonwebtoken')

const router = express.Router()

// ---------------------------------------
//                  GET
// ---------------------------------------

router.get('/listMeeting', (request, response) => {
  const statement = `select * from meeting`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.get('/salarySlip', (request, response) => {
  const statement = `select * from salary_slip where empId = ${request.id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.get('/listquery', (request, response) => {
  const statement = `select * from emp_queries where empId = ${request.id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.get('/listLeaves', (request, response) => {
  const statement = `select * from apply_leave where empId = ${request.id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ---------------------------------------
//                  POST
// ---------------------------------------

router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const statement = `select empId, empName, role from employee where email = '${email}' and password = '${password}'`
  db.query(statement, (error, admins) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (admins.length == 0) {
        response.send({ status: 'error', error: 'employee does not exist' })
      } else {
        const admin = admins[0]
        const token = jwt.sign({ id: admin['empId'] }, config.secret)
        response.send(utils.createResult(error, {
          empName: admin['empName'],
          role: admin['role'],
          token: token
        }))
      }
    }
  })
})

router.post('/applyLeave', (request, response) => {
  const { leaveDate, leavePurpose, leaveStatus } = request.body
  const statement = `insert into apply_leave ( leaveDate, leavePurpose, leaveStatus,empId) values ('${leaveDate}','${leavePurpose}','pending','${request.id}')`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.post('/addQueries', (request, response) => {
  const { queryTitle, queryDesc, queryStatus } = request.body
  const statement = `insert into emp_queries ( queryTitle, queryDesc, queryStatus,empId) values ('${queryTitle}','${queryDesc}','${queryStatus}','${request.id}')`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ---------------------------------------
//                  PUT
// ---------------------------------------

router.put('/updateProfile', (request, response) => {
  const { empName, address, birth_date, gendor, mobile } = request.body

  const statement = `update employee set 
      empName = '${empName}',
      address = '${address}',
      birth_date = '${birth_date}',
      gendor = '${gendor}',
      mobile = '${mobile}'
  where id = ${request.id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})


// ---------------------------------------
//                  DELETE
// ---------------------------------------




module.exports = router