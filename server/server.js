const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const config = require('./config')
const cors = require('cors')

// morgan: for logging
const morgan = require('morgan')


//routers
const employeeRouter = require('./routes/employee')
const adminRouter = require('./routes/admin')

const app = express()
app.use(cors('*'))

app.use(bodyParser.json())
app.use(morgan('combined'))

function getUserId(request, response, next) {

  if (request.url == '/admin/signin' 
      || request.url == '/employee/signin' ) {
    // do not check for token 
    next()
  } else {

    try {
      const token = request.headers['token']
      const data = jwt.verify(token, config.secret)

      // add a new key named userId with logged in user's id
      request.id = data['id']

      // go to the actual route
      next()
      
    } catch (ex) {
      response.status(401)
      response.send({status: 'error', error: 'protected api'})
    }
  }
}

app.use(getUserId)

//add the routes
app.use('/admin',adminRouter)
app.use('/employee',employeeRouter)

// default route
app.get('/', (request, response) => {
  response.send('welcome to my application')
})

app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})
