import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddnewemployeesComponent } from './addnewemployees/addnewemployees.component';
import { GenerateslipComponent } from './generateslip/generateslip.component';
import { LeavesComponent } from './leaves/leaves.component';
import { MeetingComponent } from './meeting/meeting.component';
import { QueriesComponent } from './queries/queries.component';
import { SeacrhemployeeComponent } from './seacrhemployee/seacrhemployee.component';

const routes: Routes = [
  { path: 'add-new-employee', component: AddnewemployeesComponent },
  { path: 'generate-slip', component: GenerateslipComponent },
  { path: 'leaves', component: LeavesComponent },
  { path: 'meetings', component: MeetingComponent },
  { path: 'queries', component: QueriesComponent },
  { path: 'search-employee', component: SeacrhemployeeComponent },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
