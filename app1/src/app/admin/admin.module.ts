import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AddnewemployeesComponent } from './addnewemployees/addnewemployees.component';
import { SeacrhemployeeComponent } from './seacrhemployee/seacrhemployee.component';
import { MeetingComponent } from './meeting/meeting.component';
import { LeavesComponent } from './leaves/leaves.component';
import { QueriesComponent } from './queries/queries.component';
import { GenerateslipComponent } from './generateslip/generateslip.component';


@NgModule({
  declarations: [AddnewemployeesComponent, SeacrhemployeeComponent, MeetingComponent, LeavesComponent, QueriesComponent, GenerateslipComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
