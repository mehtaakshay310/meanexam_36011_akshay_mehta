import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { HomeComponent } from './home/home.component';
import { LeavesComponent } from './leaves/leaves.component';
import { MeetingsComponent } from './meetings/meetings.component';
import { QueriesComponent } from './queries/queries.component';
import { SalarySlipComponent } from './salary-slip/salary-slip.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'leaves', component: LeavesComponent },
  { path: 'editProfile', component: EditProfileComponent },
  { path: 'queries', component: QueriesComponent },
  { path: 'salaryslip', component: SalarySlipComponent },
  { path: 'meetings', component: MeetingsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
