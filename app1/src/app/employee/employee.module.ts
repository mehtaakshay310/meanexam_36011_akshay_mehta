import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { HomeComponent } from './home/home.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MeetingsComponent } from './meetings/meetings.component';
import { LeavesComponent } from './leaves/leaves.component';
import { QueriesComponent } from './queries/queries.component';
import { SalarySlipComponent } from './salary-slip/salary-slip.component';


@NgModule({
  declarations: [HomeComponent, EditProfileComponent, MeetingsComponent, LeavesComponent, QueriesComponent, SalarySlipComponent],
  imports: [
    CommonModule,
    EmployeeRoutingModule
  ]
})
export class EmployeeModule { }
