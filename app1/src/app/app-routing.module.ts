import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { EmployeeHomeComponent } from './employee-home/employee-home.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { 
    path: 'home',
    component: HomeComponent,
    children:[
      { path: '', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)}
    ]
  },
  { 
    path: 'employee',
    component: EmployeeHomeComponent,
    children:[
      { path: '', loadChildren: () => import('./employee/employee.module').then(m => m.EmployeeModule)}
    ]
  },
  { 
    path: 'admin',
    component: AdminHomeComponent,
    children:[
      { path: '', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
